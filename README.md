# Entregable N 1 Base Y Estructura y TAREA 12

## 1. Base de datos clinicaDB:
script para crear base de datos.
--

    CREATE DATABASE clinicaDB;
     GO 

    USE clinicaDB;
    GO

    CREATE TABLE Dept (
     Dept_No INT PRIMARY KEY,
     DNombre NVARCHAR(50),
     Loc NVARCHAR(50)
     );
     GO
 
    CREATE TABLE Emp (
        Emp_No INT PRIMARY KEY,
        Apellido NVARCHAR(50),
        Oficio NVARCHAR(50),
        Dir INT,
        Fecha_Alt DATE,
        Salario DECIMAL(10, 2),
        Comision DECIMAL(10, 2),
        Dept_No INT,
        FOREIGN KEY (Dept_No) REFERENCES Dept(Dept_No)
     );
     GO
   
    CREATE TABLE Enfermo (
        Inscripcion INT PRIMARY KEY,
        Apellido NVARCHAR(50),
        Direccion NVARCHAR(100),
        Fecha_Nac DATE,
        S CHAR(1),
        NSS NVARCHAR(20)
     );
     GO 
    






